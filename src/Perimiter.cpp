//
//  Perimiter.cpp
//  guiPathTester
//
//  Created by Joshua Trate on 7/3/15.
//
//

#include "Perimiter.h"

void Perimeter::addPoint(Point_t p) { perim.push_back(p); }

Point_t Perimeter::getPoint() { return perim.front(); }

void Perimeter::removeNext() { perim.pop_back(); }


//  may not work
std::list<Point_t> *Perimeter::getList() { return &perim; }

// need to test
void Perimeter::loadFromFile(std::string filename)
{
    std::ifstream input(filename);
    int x, y;
    while (input >> x)
    {
        input >> y;
        Point_t temp = { static_cast<double>(x), static_cast<double>(y) };
        perim.push_back(temp);
    }
}

// need to test
void Perimeter::writeToFile(std::string filename)
{
    std::ofstream output(filename);

    // go through the points currently in the list
    for (std::list<Point_t>::iterator i = perim.begin(); i != perim.end(); ++i)
    {
        // write the current point to file
        output << i->x << " " << i->y << std::endl;
    }
}
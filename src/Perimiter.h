//
//  Perimiter.h
//  guiPathTester
//
//  Created by Joshua Trate on 7/3/15.
//
//

#ifndef __guiPathTester__Perimiter__
#define __guiPathTester__Perimiter__

#include <stdio.h>
#include <list>
#include <string>
#include <fstream>

struct Point_t
{
    double x, y;
};

class Perimeter
{
public:
    /*!
     *  adds a point to the perimeter
     *
     *  @param p the point you want to add
     */
    void addPoint(Point_t p);

    /*!
     *  returns the next point in the queue
     *
     *  @return the next point on the perimiter
     */
    Point_t getPoint();

    /*!
     *  removes the point from the perimiter
     */
    void removeNext();

    /*!
     *  load a perimiter from a file
     *
     *  @param filename file to load from
     */
    void loadFromFile(std::string filename);

    /*!
     *  write a perimiter to a file
     *
     *  @param filename the file to write too
     */
    void writeToFile(std::string filename);

    /*!
     *  gets a pointer to the list of points
     *
     *  @return a pointer to the list of points
     */
    std::list<Point_t> *getList();

private:
    /*!
     *  the points that make up the perimiter
     */
    std::list<Point_t> perim;
};

#endif /* defined(__guiPathTester__Perimiter__) */

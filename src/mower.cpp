//
//  mower.cpp
//  guiPathTester
//
//  Created by Joshua Trate on 7/2/15.
//
//

#include "mower.h"

bool PointIsInMower(const Mower &m, const Point_t &p)
{
    LGVector2D P1(m.UL.x, m.UL.y);
    LGVector2D P2(m.UR.x, m.UR.y);
    LGVector2D P3(m.LR.x, m.LR.y);
    LGVector2D P4(m.LL.x, m.LL.y);
    LGVector2D P(p.x, p.y);

    LGVector2D P1_P4 = P1 - P4;
    LGVector2D P3_P4 = P3 - P4;
    LGVector2D TWO_P_C = P * 2.0 - P1 - P3;

    return (P3_P4.dot(TWO_P_C - P3_P4) <= 0 && P3_P4.dot(TWO_P_C + P3_P4) >= 0) &&
           (P1_P4.dot(TWO_P_C - P1_P4) <= 0 && P1_P4.dot(TWO_P_C + P1_P4) >= 0);
}


bool simulationNotDone(Path &mowerPath) { return mowerPath.PathNotComplete(); }

// sin theta from vertical
double get_sin_theta(Mower *mower_in)
{
    // get top center point
    Point_t topCenter = { (mower_in->UR.x + mower_in->UL.x) / 2, (mower_in->UR.y + mower_in->UL.y) / 2 };
    Point_t bottomCenter = { (mower_in->LR.x + mower_in->LL.x) / 2, (mower_in->LR.y + mower_in->LL.y) / 2 };

    double H = sqrt((topCenter.x - bottomCenter.x) * (topCenter.x - bottomCenter.x) +
                    (topCenter.y - bottomCenter.y) * (topCenter.y - bottomCenter.y));

    // normal case
    if (topCenter.y == bottomCenter.y) return 0;

    // odd case
    return ((topCenter.y - bottomCenter.y) / H);
}

// cos from vertical
double get_cos_theta(Mower *mower_in)
{
    Point_t topCenter = { (mower_in->UR.x + mower_in->UL.x) / 2, (mower_in->UR.y + mower_in->UL.y) / 2 };
    Point_t bottomCenter = { (mower_in->LR.x + mower_in->LL.x) / 2, (mower_in->LR.y + mower_in->LL.y) / 2 };
    double H = sqrt((topCenter.x - bottomCenter.x) * (topCenter.x - bottomCenter.x) +
                    (topCenter.y - bottomCenter.y) * (topCenter.y - bottomCenter.y));

    // special case
    if (topCenter.x - bottomCenter.x == 0) return 0;

    // normal case
    return ((topCenter.x - bottomCenter.x) / H);
}


// moves the center of the mower the given distance in pixles forward
void move_mower(Mower *mower_in, int move_distance)
{
    double cosTheta = get_cos_theta(mower_in);
    double sinTheta = get_sin_theta(mower_in);

    // update upper left
    mower_in->UL.x = mower_in->UL.x + move_distance * cosTheta;
    mower_in->UL.y = mower_in->UL.y + move_distance * sinTheta;
    // update upper rignt
    mower_in->UR.x = mower_in->UR.x + move_distance * cosTheta;
    mower_in->UR.y = mower_in->UR.y + move_distance * sinTheta;
    // update lower left
    mower_in->LL.x = mower_in->LL.x + move_distance * cosTheta;
    mower_in->LL.y = mower_in->LL.y + move_distance * sinTheta;
    // update lower right
    mower_in->LR.x = mower_in->LR.x + move_distance * cosTheta;
    mower_in->LR.y = mower_in->LR.y + move_distance * sinTheta;
}
// rotates point around given axis
Point_t pointAroundAxis(Point_t const &axis, Point_t const &pt, const double degrees)
{
    double angle = (degrees) * (M_PI / 180.0); // Convert to radians
    Point_t newpt;
    newpt.x = (cos(angle) * (pt.x - axis.x) - sin(angle) * (pt.y - axis.y) + axis.x);
    newpt.y = (sin(angle) * (pt.x - axis.x) + cos(angle) * (pt.y - axis.y) + axis.y);

    return newpt;
}

// turns the mower the given distance
void turn_mower(Mower *mower_in, double degrees)
{
    // finds rotation point
    Point_t aor = { ((mower_in->LL.x + mower_in->LR.x) / 2.0), ((mower_in->LL.y + mower_in->LR.y) / 2.0) };

    // Moves UL point
    mower_in->UL = pointAroundAxis(aor, mower_in->UL, degrees);
    // Moves UR point
    mower_in->UR = pointAroundAxis(aor, mower_in->UR, degrees);
    // Moves LL Point
    mower_in->LL = pointAroundAxis(aor, mower_in->LL, degrees);
    // Moves LR point
    mower_in->LR = pointAroundAxis(aor, mower_in->LR, degrees);
}

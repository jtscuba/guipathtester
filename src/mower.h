//
//  mower.h
//  guiPathTester
//
//  Created by Joshua Trate on 7/2/15.
//
//

#ifndef __guiPathTester__mower__
#define __guiPathTester__mower__

#include <stdio.h>
#include "Path.h"
#include "LGVector2D.h"

// structs
struct Mower
{
    Point_t UL, UR, LL, LR;
    int r, g, b; // color of squares that get covered
};

// the squares the background are made out of
struct GridSquare
{
    int xCorner, yCorner;
    int xLength, yLength;

    bool beenInMower = false;
};


/*!
 *
 *
 *  @param m a mower struct
 *  @param p a point
 *
 *  @return true if the point is instide the mower
 */
bool PointIsInMower(const Mower &m, const Point_t &p);

/*!
 *  checks if the simulation is done yet
 *
 *  @param mowerPath the path currently being used in simulatuion
 *
 *  @return true if there are still steps on the path
 */
bool simulationNotDone(Path &mowerPath);

/*!
 *  gets the sin of the mowers current angle from vertical
 *
 *  @param mower_in any mower currently initalized
 *
 *  @return a double representing the sign
 */
double get_sin_theta(Mower *mower_in);
/*!
 *  gets the cos of the mowers current angle from vertical
 *
 *  @param mower_in any mower currently initalized
 *
 *  @return a double representing the sign
 */
double get_cos_theta(Mower *mower_in);

/*!
 *  moves the mower the given distance forward
 *
 *  @param mower_in      any mower thats been initalized
 *  @param move_distance the number of pixels to move forward
 */
void move_mower(Mower *mower_in, int move_distance);

/*!
 *  returns the new coordinates of a point that has been rotated a given amount around a given axis
 *
 *  @param axis    the axis of rotation
 *  @param pt      a point you want to move
 *  @param degrees how many degrees you want to rotate the point
 *
 *  @return the new coordinates of the point
 */
Point_t pointAroundAxis(Point_t const &axis, Point_t const &pt, const double degrees);

/*!
 *  turnst a mower the given number of degrees
 *
 *  @param mower_in any mower with coordinates
 *  @param degrees  number of degrees to turn
 */
void turn_mower(Mower *mower_in, double degrees);

#endif /* defined(__guiPathTester__mower__) */

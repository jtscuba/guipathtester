#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include <cmath>
#include <unistd.h>
#include <vector>
#include <list>
#include <cassert>
#include <iostream>
#include <algorithm>
#include "LGVector2D.h"
#include <cstring>
#include "Path.h"
#include "mower.h"
#include <boost/timer.hpp>


using namespace ci;
using namespace ci::app;
using namespace std;


class guiPathTesterApp : public AppNative
{

public:
    // basic setup
    void prepareSettings(Settings *settings);
    void setup();
    void update();
    void draw();
    void shutdown();

    // should be member function
    void updateSimulation();

private:
    int framecount;

    vector<vector<GridSquare *>> UnhitGrid;
    list<GridSquare *> hitSquares;

    Mower mower;
    Path mowerPath;
    Perimeter perim;
    Timer t;
};

void guiPathTesterApp::prepareSettings(Settings *settings)
{
    settings->setWindowSize(1400, 900);
    settings->setFrameRate(120.0f);
}

void guiPathTesterApp::setup()
{

    vector<vector<GridSquare *>> temp(1400 / 4, vector<GridSquare *>(225));
    UnhitGrid = temp;

    Point_t ul = { 100, 100 };
    Point_t ur = { 140, 100 };
    Point_t ll = { 100, 160 };
    Point_t lr = { 140, 160 };

    mower = { ul, ur, ll, lr };

    // load perim

    fs::path filename = getOpenFilePath();

    perim.loadFromFile(filename.generic_string());

    mowerPath = Path(perim);

    mowerPath.computePath();

    // initalizes grid
    assert(UnhitGrid.size() == 1400 / 4);
    assert(UnhitGrid[2].size() == 225);

    // initalizes grid
    for (int x = 0; x < 1400 / 4; ++x)
    {
        for (int y = 0; y < 900 / 4; ++y)
        {
            UnhitGrid[x][y] = new GridSquare;
            // start at 0,0 instead of negative
            (UnhitGrid[x][y])->xCorner = 4 * x;
            (UnhitGrid[x][y])->yCorner = 4 * y;
            (UnhitGrid[x][y])->xLength = 4;
            (UnhitGrid[x][y])->yLength = 4;
        }
    }
    assert(UnhitGrid[0][0]->xCorner == 0);
    assert(UnhitGrid[0][0]->yCorner == 0);

    framecount = 0;
}

// mower needs to be a local variable not passed

void guiPathTesterApp::update()
{
    t.start();
    // should move everything
    cout << simulationNotDone(mowerPath) << endl;
    if (simulationNotDone(mowerPath)) updateSimulation();
}

void guiPathTesterApp::draw()
{

    // clear out the window with white
    gl::clear(Color(1, 1, 1));

    // estimate frame rate
    cout << "frame count" << framecount++ << endl;


    // draw grid with lines
    gl::lineWidth(.5);
    gl::color(.7, .7, .7);
    for (int x = 0; x < 1400 / 4 + 1; ++x)
    {
        Vec2f start(x * 4, 0);
        Vec2f end(x * 4, 900);

        gl::drawLine(start, end);
    }

    for (int y = 0; y < 900 / 4 + 1; ++y)
    {
        Vec2f start(0, y * 4);
        Vec2f end(1400, y * 4);

        gl::drawLine(start, end);
    }

    // draw colored squares
    Rectf rect;

    gl::color(.3, .8, .3);
    for (list<GridSquare *>::iterator i = hitSquares.begin(); i != hitSquares.end(); ++i)
    {
        // draw squares
        rect = Rectf((*i)->xCorner, (*i)->yCorner, (*i)->xCorner + (*i)->xLength, (*i)->yCorner + (*i)->yLength);
        gl::drawSolidRect(rect);
    }


    // draw perimiter
    gl::color(0, 0, 1);
    list<Point_t> *perimList = perim.getList();

    Point_t temp = perim.getPoint();

    for (list<Point_t>::iterator i = perimList->begin(); i != perimList->end(); ++i)
    {

        Vec2d start = { temp.x, temp.y };
        Vec2d end = { i->x, i->y };

        gl::drawLine(start, end);
        temp = *i;
    }

    // sets color
    gl::color(1, 0, 0);

    // makes a rect where the mower is
    Vec2f UL(mower.UL.x, mower.UL.y);
    Vec2f UR(mower.UR.x, mower.UR.y);
    Vec2f LR(mower.LR.x, mower.LR.y);
    Vec2f LL(mower.LL.x, mower.LL.y);

    // draws mower
    gl::drawLine(UL, UR);
    gl::drawLine(UR, LR);
    gl::drawLine(LR, LL);
    gl::drawLine(LL, UL);

    // marks axis of rotation
    Vec2f axisOfRotation((mower.LL.x + mower.LR.x) / 2, (mower.LL.y + mower.LR.y) / 2);
    gl::drawSolidCircle(axisOfRotation, 5);

    t.stop();
    cout << "aprox fps " << 1 / t.getSeconds() << endl;
}

void guiPathTesterApp::shutdown()
{
    // memory cleanup
    cout << "cleaning up memory" << endl;
    for (int x = 0; x < 1400 / 4; ++x)
    {
        for (int y = 0; y < 900 / 4; ++y)
        {
            delete UnhitGrid[x][y];
        }
    }
}

void guiPathTesterApp::updateSimulation()
{
    // gets bounding rectangle
    int xmax = max(max(max(mower.UL.x, mower.UR.x), mower.LL.x), mower.LR.x);
    int xmin = min(min(min(mower.UL.x, mower.UR.x), mower.LL.x), mower.LR.x);
    int ymax = max(max(max(mower.UL.y, mower.UR.y), mower.LL.y), mower.LR.y);
    int ymin = min(min(min(mower.UL.y, mower.UR.y), mower.LL.y), mower.LR.y);

    // updates color of squares
    //    cout << "Checking the grid squares" << endl;
    //
    //    cout << "xmin = " << xmin << endl;
    //    cout << "xmax = " << xmax << endl;
    //    cout << "ymin = " << ymin << endl;
    //    cout << "ymax = " << ymax << endl;

    bool analysis = false;
    for (int x = xmin / 4; x <= xmax / 4 + 4; ++x)
    {
        for (int y = ymin / 4; y <= ymax / 4 + 4; ++y)
        {
            Point_t p = { static_cast<double>(UnhitGrid[x][y]->xCorner), static_cast<double>(UnhitGrid[x][y]->yCorner) };
            // if the point is in the mower and the point has never been in the mower before
            if (UnhitGrid[x][y]->beenInMower == false && PointIsInMower(mower, p))
            {
                // add to list
                hitSquares.push_back(UnhitGrid[x][y]);

                UnhitGrid[x][y]->beenInMower = true;

                // tests to see if the loop is run
                if (!analysis) analysis = true;
            }
        }
    }

    // print results of analysis
    if (analysis)
        cout << "loop was true" << endl;
    else
        cout << "loop was false" << endl;

    // updates mower position
    movement nextMove = mowerPath.getNextstep();

    if (nextMove.move == drive)
    {
        move_mower(&mower, nextMove.value.distance);
    }

    else
    {
        turn_mower(&mower, nextMove.value.angle);
    }
}


CINDER_APP_NATIVE(guiPathTesterApp, RendererGl)

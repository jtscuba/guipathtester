//
//  PerimiterMaker.cpp
//  guiPathTester
//
//  Created by Joshua Trate on 7/3/15.
//
//

#include <stdio.h>
#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "Perimiter.h"
#include <fstream>
#include <list>


using namespace ci;
using namespace ci::app;
using namespace std;


class PerimiterMaker : public AppNative
{


public:
    // basic setup
    void prepareSettings(Settings *settings);
    void setup();
    void update();
    void draw();
    void shutdown();
    void mouseDown(MouseEvent event);
    void keyDown(KeyEvent event);

private:
    Perimeter perim;
    Point_t lastPoint, longestStart, longestEnd;
    bool drawShortestline = false;
};

void PerimiterMaker::prepareSettings(Settings *settings)
{
    settings->setWindowSize(1400, 900);
    settings->setFrameRate(30.0f);
}

void PerimiterMaker::setup() {}

void PerimiterMaker::update()
{
    // measure from new point to all other points

    // compare to old max

    // less than 2 points it doesn't run

    // when 2 points only, measures from 2nd to first

    // when point is added measure from new point to all others

    // when three or more points are present, pseudo complete the perimiter and measure, also check that point is in
    // polygon and doesn't cross any boundaries

    // watch out for closingthe path early, it may cause the lines to wrongly intersect,
}

void PerimiterMaker::draw()
{
    gl::clear(Color(1, 1, 1));

    // draw the background grid
    gl::lineWidth(.5);
    gl::color(.7, .7, .7);
    for (int x = 0; x < 1400 / 4 + 1; ++x)
    {
        Vec2f start(x * 4, 0);
        Vec2f end(x * 4, 900);

        gl::drawLine(start, end);
    }

    for (int y = 0; y < 900 / 4 + 1; ++y)
    {
        Vec2f start(0, y * 4);
        Vec2f end(1400, y * 4);

        gl::drawLine(start, end);
    }

    // draw the current lines

    list<Point_t> *perimList = perim.getList();

    Point_t temp = perim.getPoint();

    // draw the points
    // test
    gl::color(0, 0, 1);
    for (list<Point_t>::iterator i = ++perimList->begin(); i != perimList->end(); ++i)
    {
        Vec2f start(temp.x, temp.y);
        Vec2f end(i->x, i->y);
        gl::drawLine(start, end);
        temp = *i;
    }

    // draw shortest line
    gl::color(1, 0, 0);
    if (drawShortestline)
    {
        Vec2f start(longestStart.x, longestStart.y);
        Vec2f end(longestEnd.x, longestEnd.y);
        gl::drawLine(start, end);
    }
}

void PerimiterMaker::mouseDown(MouseEvent event)
{
    // if mouse is clicked, add point to perimiter
    if (event.isLeft() && !drawShortestline)
    {
        lastPoint = { static_cast<double>(event.getX()), static_cast<double>(event.getY()) };


        // check lines from the new point to every other point
        list<Point_t> *perimlist = perim.getList();

        for (list<Point_t>::iterator i = perimlist->begin(); i != perimlist->end(); ++i)
        {
            double currentLongestSquared = (longestStart.x - longestEnd.x) * (longestStart.x - longestEnd.x) +
                                           (longestStart.y - longestEnd.y) * (longestStart.y - longestEnd.y);

            double distanceSquared = (i->x - lastPoint.x) * (i->x - lastPoint.x) + (i->y - lastPoint.y) * (i->y - lastPoint.y);

            if (distanceSquared > currentLongestSquared)
            {
                longestStart = *i;
                longestEnd = lastPoint;
            }
        }
        perim.addPoint(lastPoint);
    }
}

// if e is pressed, add last point to perimiter to close the path and write to file
void PerimiterMaker::keyDown(KeyEvent event)
{
    if (event.getChar() == 'f')
    {
        if (perim.getPoint().x != lastPoint.x || perim.getPoint().y != lastPoint.y)
        {
            perim.addPoint(perim.getPoint());
        }
        // gets file location
        fs::path file = getSaveFilePath();

        if (file.generic_string() != "")
        {
            // write to file
            perim.writeToFile(file.generic_string());
            // exit the app
            quit();
        }
    }
    if (event.getChar() == 'd')
    {
        // duplicate the starting point if necessary
        if (perim.getPoint().x != lastPoint.x || perim.getPoint().y != lastPoint.y)
        {
            perim.addPoint(perim.getPoint());
        }

        drawShortestline = true;
    }

    if (event.getChar() == 'r')
    {
        // reset the display
        drawShortestline = false;
        longestStart = { 0, 0 };
        longestEnd = { 0, 0 };
        lastPoint.x = NULL;
        lastPoint.y = NULL;

        perim = Perimeter();
    }
}

void PerimiterMaker::shutdown() { cout << "cleanup" << endl; }

CINDER_APP_NATIVE(PerimiterMaker, RendererGl)
